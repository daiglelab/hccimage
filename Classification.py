#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 14:26:04 2019

@author: llu
"""

import numpy as np
import pandas as pd
from scipy import interp
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve
import matplotlib.pyplot as plt
#from sklearn.utils.fixes import signature
from sklearn.metrics import average_precision_score
from matplotlib.pyplot import figure

from sklearn.ensemble import RandomForestClassifier
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure

import os
os.chdir("/home/llu/HardDisk/Bitbucket/HCC_image/results/Classification")
# #############################################################################


def Clinical_df():
    
    filename = "../HCC_clinical/HCC_clinicalInfo.txt"
    out = pd.read_table(filename)
    return(out)


def obtain_npy(model = "VGG", FS_len = 2048, crop = 512):
    # read npy and obtain median value for each sample
    # save as pandas Data Frame
    dirname =  '../Feature_extraction_Level_2/' + model + "-0.5-" + str(crop) + "/"
    allfile = os.listdir(dirname)
    
    if model == "VGG":
        FS_len = 1408
    
    df = pd.DataFrame(index = range(FS_len), columns = [a[:16] for a in allfile])
    
    for i, name in enumerate(allfile):
        data = np.load(dirname + name)
        media = np.median(data, axis=0)
        #media = np.mean(data, axis=0)
        
        df.iloc[:, i] = media.tolist()

    return(df.T)



def Classification_perf(df, model = "VGG",  crop = 512, name_out = "test"):

    labels = [ i[13:15] for i in df.index.tolist()]
    target_ids = labels

    names = df.index.tolist()
    clinical = Clinical_df()
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    #target_ids = np.random.randint(2, size= 288)
    ###draw Cancer type
    target_ids = ["Normal" if one[13:15] == '11' else "Cancer" for one in df.index.tolist() ]
    target_ids = np.array(target_ids)
    target_names = np.unique(target_ids).tolist()
        
    x = StandardScaler().fit_transform(df)
    pca = PCA(n_components=10)
    principalComponents = pca.fit_transform(x)
    pca.explained_variance_ratio_
    principalDf = principalComponents
    
    if model == "PCA" or model == "SVD":
        principalDf = df.iloc[:, :10]

    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(principalDf)
    X_2d.shape
        
    figure(num=None, figsize=(21, 6), dpi=80, facecolor='w', edgecolor='k')
    
    ax1 = plt.subplot(131)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    #ax1.set_title(model + '_PCA10_T-SNE_' + str(crop) + '_DataType')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    #ax1.legend(loc='upper center')
    
    ##pure T-SNE
    tsne = TSNE(n_components=2, random_state=0)
    X_2d2 = tsne.fit_transform(df)
    X_2d2.shape
    
    
    ###classification
    X = np.array(df); y = np.array([1 if one == "Cancer" else 0 for one in target_ids])
    
    random_state = np.random.RandomState(0)
    cv = StratifiedKFold(n_splits=6)
    #cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    
    ##use linear SVM
    classifier = svm.SVC(kernel='linear', probability=True, random_state=random_state)
    ## use RF
    #classifier = RandomForestClassifier(random_state=random_state)
    
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    
    ax1 = plt.subplot(132)
    
    i = 0
    for train, test in cv.split(X, y):

        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        ax1.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
        
    ax1.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    
    ax1.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC(AUC=%0.2f$\pm$%0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax1.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$1 std. dev.')
    
    ax1.set_xlim([-0.05, 1.05])
    ax1.set_ylim([-0.05, 1.05])
    ax1.set_xlabel('False Positive Rate')
    ax1.set_ylabel('True Positive Rate')
    #ax1.set_title('Receiver operating characteristic')
    ax1.legend(loc="lower right")
    #ax1.show()

    '''
    # Limit to the two first classes, and split into training and test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.2, random_state=random_state)
    
    #classifier = svm.LinearSVC(random_state=random_state)
    
    classifier = svm.SVC(kernel='linear', probability=True, random_state=random_state)
    
    #classifier = RandomForestClassifier(random_state=random_state)
    
    classifier.fit(X_train, y_train)
    y_score = classifier.decision_function(X_test) # for SVM
    
    #y_score = classifier.predict_proba(X_test)
    
    average_precision = average_precision_score(y_test, y_score)
    
    print('Average precision-recall score: {0:0.2f}'.format(average_precision))
    
    print("check point: ", y_test, y_score)
    
    precision, recall, _ = precision_recall_curve(y_test, y_score)
    # In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
    step_kwargs = ({'step': 'post'}
                   if 'step' in signature(plt.fill_between).parameters
                   else {})
    
    ax1 = plt.subplot(133)
    ax1.step(recall, precision, color='b', alpha=0.2,
             where='post')
    ax1.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)
    
    ax1.set_xlabel('Recall')
    ax1.set_ylabel('Precision')
    ax1.set_ylim([0.0, 1.05])
    ax1.set_xlim([0.0, 1.0])
    #ax1.set_title('2-class Precision-Recall curve: AP={0:0.2f}'.format(average_precision))

    '''
    plt.rc('font', size = 24 )          # controls default text sizes
    plt.rc('axes', titlesize = 24)     # fontsize of the axes title
    plt.rc('axes', labelsize = 24)    # fontsize of the x and y labels
    plt.rc('legend', fontsize= 12 )
    #plt.rc('xtick', labelsize = 24)     
    #plt.rc('ytick', labelsize = 24)      
    
    plt.tight_layout()
    #plt.show()
    plt.savefig('../Classification/' + name_out + "_classification.pdf")
    
    #plt.savefig('../Classification/' + name_out + "_classification_RF.pdf")

    
    #return (fig)



if __name__ == "__main__":
    
    models = ["VGG", "Inception", "ResNet"]
    crops = [256, 512]
    
    for model in models:
        for crop in crops:

            #crop = crops[0]
            #model = "VGG"; crop = 256
            df = obtain_npy(model, crop = crop)
            #TSNE_plot(df, model, crop = crop)
            Classification_perf(df, model, crop = crop, name_out = model + '_' + str(crop) )
        
        











