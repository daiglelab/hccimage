#!/usr/bin/R
"""
This script aims at HCC image analysis; mainly survival curves for stages, grades and quantitative features association 
"""
source("func.R")

workdir <- "/home/llu/HardDisk/TCGA_HC_image/results"
datadir <- "/home/llu/HardDisk/TCGA_HC_image/HCC_data/"

##Download TCGA_LIHC clinical data
if(FALSE){
    TCGA_clinical_files()
}

###sample statistics; Characteristics and Summary
if(TRUE){
    
    cli_hcc <- read.table(paste0(datadir, "nationwidechildrens.org_clinical_patient_lihc.txt"), sep = "\t", stringsAsFactors = F, fill = T, header = T, quote = "")
    #dim(cli_hcc)
    idx = c("bcr_patient_barcode", "age_at_diagnosis", "tumor_grade", "ajcc_pathologic_tumor_stage", "vital_status", "death_days_to", "last_contact_days_to", "histologic_diagnosis", "tumor_status", "new_tumor_event_dx_indicator")
    tmp <- cli_hcc[, idx]
    tmp <- tmp[3:nrow(tmp), ]
    colnames(tmp) <- c("Patient_barcode", "Age", "Grade", "Stage", "OS_event", "death_days_to", "last_contact_days_to", "histologic_diagnosis", "DFS_event", "DFS_days")
    #normalize grade and stage
    tmp$Grade[tmp$Grade == "[Not Available]"] = "Not Available"
    #tmp$Stage = gsub("Stage IVA", "Stage IV", tmp$Stage)
    #tmp$Stage = gsub("Stage IVB", "Stage IV", tmp$Stage)
    #tmp$Stage = gsub("Stage III*", "Stage III", tmp$Stage) 
    tmp$Stage[tmp$Stage == "[Discrepancy]"] = "Not Available"
    tmp$Stage[tmp$Stage == "[Not Available]"] = "Not Available"
    print(table(tmp$Stage))
    print(table(tmp$Grade))

}

###prognosis analysis
library(survival)
library(survminer)

fit <- survfit(Surv(OS_days, event = factor(OS_event)) ~ factor(stage),
               data = tmp)
summary(fit)

ggsurvplot(fit, data = tmp, pval = TRUE, risk.table = TRUE)

survdiff(Surv(tmp$OS_days, factor(tmp$OS_event)) ~ factor(tmp$stage))

# Fit a Cox proportional hazards model
fit.coxph <- coxph(Surv(tmp$OS_days, factor(tmp$OS_event)) ~ factor(tmp$stage))

summary(fit.coxph)

ggforest(fit.coxph, data = tmp)


