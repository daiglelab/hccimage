import os, sys
import pandas as pd
from collections import Counter

os.chdir("/home/llu/HardDisk/TCGA_HC_image/results/HCC_clinical")

file = "/home/llu/HardDisk/TCGA_HC_image/HCC_data/Diagnosis_slide/gdc_manifest.2019-01-23.txt"

df = pd.read_table(file)
filenames = df['filename'].tolist()
filenames = [one[:16] for one in filenames]

tissues = [one.split("-")[3] for one in filenames ]
out = pd.DataFrame(columns = ["barcode_sample", "Count"])
out["barcode_sample"] = list(Counter(tissues).keys())
out["Count"] = list(Counter(tissues).values())

tmp = [one[:12] for one in filenames]
len(set(tmp))

normals = [one[:12] for one in filenames if '11A' in one]
len(set(normals))

tmp = [one[:12] for one in filenames if '11A' not in one]
len(set(tmp))

# barcode with both 01 and 11
cmm = set(normals).intersection(set(tmp))


"""
barcode_sample  Count
0            01A    441
1            01Z    415
2            11A    105
3            01B      2
4            02A      2
5            02B      1
"""
out.to_csv("HCC_barcode_count.csv")

# unique sample with images
samples = set([one[:12] for one in filenames])
tmp = pd.DataFrame(index = samples, columns = list(Counter(tissues).keys()) )
for one in tmp.columns.tolist():
    for idx in tmp.index.tolist():    
        ss = idx + "-" + one
        ss = [k for k in df['filename'].tolist() if k.startswith(ss)]
        tmp.loc[idx, one] = len(ss)
           
tmp.sum()
"""
            01A  01Z  11A  01B  02A  02B
TCGA-G3-A25W    1    1    1    0    0    0
TCGA-RG-A7D4    1    1    0    0    0    0
TCGA-DD-AAVR    1    1    0    0    0    0
TCGA-G3-AAV1    1    1    0    0    0    0
TCGA-ED-A82E    1    1    0    0    0    0
TCGA-G3-AAV5    1    1    0    0    0    0

"""

#visual dimentions

# count image shape
import openslide
import pandas as pd
import numpy as np
from openslide.deepzoom import DeepZoomGenerator

svspath = '/home/llu/HardDisk/TCGA_HC_image/' + 'SVS_Data/'
allfiles = os.listdir(svspath)
df = pd.DataFrame(index = range(len(allfiles)), columns = ['TCGA number', 'SVS code', 'Level', 'Level dim', 'Downsample dim', 'Tiles', 'Tile dim'])
for i in range(len(allfiles)):
    code = allfiles[i]
    #code = 'fa5919e4-2398-483a-ae51-f667646e1e3c'
    #print(os.listdir('SVS_Data/fa5919e4-2398-483a-ae51-f667646e1e3c'))
    one = os.listdir(svspath + code)
    one = [ n for n in one if 'svs' in n]     
    filename = svspath + code + '/' + one[0]
    test = openslide.open_slide(filename)
    
    downsamples=test.level_downsamples
    [w,h]=test.level_dimensions[0]

    print(downsamples)
    print(test.level_dimensions)
    size1=int(w*(downsamples[0]/downsamples[-1]))
    size2=int(h*(downsamples[0]/downsamples[-1]))
    region=np.array(test.read_region((0,0),len(downsamples) -1 ,(size1,size2)))
    img = region
    
    tiles = DeepZoomGenerator(test)
    #print(tiles.level_count, tiles.level_tiles, tiles.level_dimensions)
    dim = tiles.level_dimensions[-1]
    level = tiles.level_count
    tile = tiles.level_tiles[-1]
    tile_count = tiles.tile_count
    test.close()
    
    print(str(dim), str((size1,size2)))
    df.iloc[i, :] = [one, code, level, str(dim), str((size1,size2)), tile_count, tile]

print(df.shape)
     
import re
import numpy as np
from scipy.stats import iqr
##process x and y pixel

def obtain_pp(column = df['Level dim'].tolist()):

    xy = column
    print(xy[:5])

    #xy = [i.replace('()', '') for i in xy]
    xy = [re.sub('[(|)]', '', i) for i in xy]
    xx = [ int(i.split(', ')[0]) for i in xy]
    yy = [ int(i.split(', ')[1]) for i in xy]
    #xx = iqr(np.array(xx))
    print([np.max(xx), np.min(xx), np.median(xx)])
    print([np.max(yy), np.min(yy), np.median(yy)])
    
    return(xx, yy)

xx1, yy1 = obtain_pp(column = df['Level dim'].tolist())
xx2, yy2 = obtain_pp(column = df['Downsample dim'].tolist())


import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="ticks")

%matplotlib inline 

ax = plt.subplot(211)

ax.scatter(xx, yy, c=df['Level'].tolist())
ax.legend(df['Level'].tolist())
ax.set_ylabel("y pixels")
ax.set_xlabel("x pixels")

ax = plt.subplot(212)
ax.scatter(xx, yy, c=df['Level'].tolist())
ax.legend(df['Level'].tolist())
ax.set_ylabel("y pixels Down")
ax.set_xlabel("x pixels Down")

plt.tight_layout()
plt.show()
    
plt.savefig("image_downsample.pdf")   # save the figure to file
plt.close()    # close the figure










