# Code Repository For HCC Image Project

## Availability of data and material
> All datasets analyzed during the current study were previously generated and are available from the National Cancer Institute Genomic Data Commons Data Portal (https://portal.gdc.cancer.gov), the cBioPortal for Cancer Genomics (https://www.cbioportal.org), and the Broad Institute GDAC Firehose (https://gdac.broadinstitute.org). Source code implementing the described analyses is available in this repository.


## Python3 Notebook Files

Feature_extraction_ipython.ipynb
hist_openCV.ipynb
PyWSI.ipynb
SVS_Image.ipynb
test_image.ipynb
test.ipynb
test2_image.ipynb

## Python3 Code
SVM classfication: Classification.py 
K-Means Clustering: clustering.py
VGG16 Feature Mapping: CNN_FeatureMap.py
IPL and Image Feature Correlation: Correlation_img_genes.py
Image Feature Extraction: feature_extractor.py
ResNeXt50 Model: MakeResNeXt.py
Try Models: models_RandomWeight.py
Try Models: models_smallNetwork.py
Try Models: models_VGG16DropOut.py
Try Models: models_visualCAMandFeatures.py
Pre-Trained Models: models.py
IPL Data: RNASeq_preprocess.py
Visualization: T-SNE_visual-Review2nd.py
Visualization: T-SNE_visual.py
Samples: TCGA_samples.py
Threads: threaded_generator.py


## R code

Differential Expression: DE_IPL.R
doResults.R
func.R
Survival: groupSurvival.R
Survival: Subgroup_survival.R
Survival: Survival_model.R
TCGA Clinical Information: TCGA_stage.R
