#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 14:06:19 2019

@author: llu
"""

import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure

import os
os.chdir("/home/llu/HardDisk/Bitbucket/HCC_image/")
import Classification
from Classification import Classification_perf

import os
os.chdir("/home/llu/HardDisk/TCGA_HC_image/results/image_features")


def Clinical_df():
    
    filename = "../HCC_clinical/HCC_clinicalInfo.txt"
    out = pd.read_table(filename)
    return(out)


def obtain_npy(model = "VGG", FS_len = 2048, crop = 512):
    # read npy and obtain median value for each sample
    # save as pandas Data Frame
    if model == "PCA" or model == "SVD":
        dirname =  '../Feature_extraction_Level_2/' + model + "100" + "/"
    else:
        dirname =  '../Feature_extraction_Level_2/' + model + "-0.5-" + str(crop) + "/"
        
    allfile = os.listdir(dirname)
    
    if model == "VGG":
        FS_len = 1408
    
    if model == "MobileNet":
        FS_len = 1024
        
    if model == "ResNeXt":
        FS_len = 2048
        
           
    if model == "PCA" or model == "SVD":
        FS_len = 100        
    
    
    df = pd.DataFrame(index = range(FS_len), columns = [a[:16] for a in allfile])
    #get first features from augmentation
    #df2 = pd.DataFrame(index = range(FS_len), columns = [a[:16] for a in allfile])
    for i, name in enumerate(allfile):
        data = np.load(dirname + name)
        media = np.median(data, axis=0)
        #media = np.mean(data, axis=0)
        df.iloc[:, i] = media.tolist()

        #df2.iloc[:, i] = data[0].tolist()
        ##df2 
        
    return(df.T)
    
    
    

def obtain_npy_NoAUG(model = "VGG", FS_len = 2048, crop = 512):
    # read npy and obtain median value for each sample
    # save as pandas Data Frame
    dirname =  '../Feature_extraction_NoAUG/' + model + "-0.5-" + str(crop) + "/"
        
    allfile = os.listdir(dirname)
    
    if model == "VGG":
        FS_len = 1408
          
    df = pd.DataFrame(index = range(FS_len), columns = [a[:16] for a in allfile])
    #get first features from augmentation
    df2 = pd.DataFrame(index = range(FS_len), columns = [a[:16] for a in allfile])
    for i, name in enumerate(allfile):
        data = np.load(dirname + name)
        media = np.median(data, axis=0)
        #media = np.mean(data, axis=0)
        #df.iloc[:, i] = media.tolist()

        df2.iloc[:, i] = media.tolist()

    return(df2.T)



def TSNE_plot(df, model = "VGG", crop = 512, name_out = "test"):
    
    labels = [ i[15] for i in df.index.tolist()]
    target_ids = labels

    names = df.index.tolist()
    clinical = Clinical_df()
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    #target_ids = np.random.randint(2, size= 288)
    ###draw Cancer type
    target_ids = ["Normal" if one[13:15] == '11' else "Cancer" for one in df.index.tolist() ]
    target_ids = np.array(target_ids)
    target_names = np.unique(target_ids).tolist()
    
    ## add classification
    #tmp = Classification_perf(df, target_ids)
    
    
    #PCA check varies
    x = StandardScaler().fit_transform(df)
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    pca.explained_variance_ratio_
    principalDf = np.array(principalDf)
    # array([ 0.19312921,  0.15181227])
    
    figure(num=None, figsize=(10, 12), dpi=80, facecolor='w', edgecolor='k')
    
    ax1 = plt.subplot(321)
    ax1.set_xlabel('Principal Component 1', fontsize = 15)
    ax1.set_ylabel('Principal Component 2', fontsize = 15)
    ax1.set_title('2 component PCA', fontsize = 20)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(principalDf[target_ids == j, 0], principalDf[target_ids == j, 1], c = k, label = j )
    
    ax1.set_xlabel( "PCA1 {0:.1%}".format(pca.explained_variance_ratio_[0]) )
    ax1.set_ylabel( "PCA2 {0:.1%}".format(pca.explained_variance_ratio_[1]) )    
    ax1.set_title(model + '_PCA_' + str(crop) + '_DataType')
    ax1.legend()
    

    x = StandardScaler().fit_transform(df)
    pca = PCA(n_components=10)
    principalComponents = pca.fit_transform(x)
    pca.explained_variance_ratio_
    principalDf = principalComponents
    
    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(principalDf)
    X_2d.shape
    
    ax1 = plt.subplot(322)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    ax1.set_title(model + '_PCA10_T-SNE_' + str(crop) + '_DataType')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ##pure T-SNE
    tsne = TSNE(n_components=2, random_state=0)
    X_2d2 = tsne.fit_transform(df)
    X_2d2.shape
    
    ax1 = plt.subplot(326)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d2[target_ids == j, 0], X_2d2[target_ids == j, 1], c = k, label = j )
    
    ax1.set_title(model + '_T-SNE_' + str(crop) + '_DataType')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)

    ###draw stage
    target_ids = [ clinical.loc[clinical['Samples'] == one[:12], 'Stage'].tolist()[0] if one[:12] in clinical['Samples'].tolist() else np.NaN for one in names ]
    target_ids = [one if not pd.isnull(one) else "No Stage" for one in target_ids]
    target_ids = np.array(target_ids)
    cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    target_ids[cancers == "Normal"] = "Normal"
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['Stage I', 'Stage II', 'Stage III', 'Stage IV']    
    ax2 = plt.subplot(323)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' + str(crop) + "_Stage" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ###draw grade
    target_ids = [ clinical.loc[clinical['Samples'] == one[:12], 'Grade'].tolist()[0] if one[:12] in clinical['Samples'].tolist() else np.NaN for one in names ]
    target_ids = [one if not pd.isnull(one) else "No Grade" for one in target_ids]
    target_ids = np.array(target_ids)
    cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    target_ids[cancers == "Normal"] = "Normal"
    
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['G1', 'G2', 'G3', "G4"]
    
    ax2 = plt.subplot(324)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' + str(crop) + "_Grade" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    # draw OS event
    target_ids = [ clinical.loc[clinical['Samples'] == one[:12], 'OS_event'].tolist()[0] if one[:12] in clinical['Samples'].tolist() else np.NaN for one in names ]
    target_ids = [one if not pd.isnull(one) else "No Event" for one in target_ids]
    target_ids = np.array(target_ids)
    cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    target_ids[cancers == "Normal"] = "Normal"
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['Alive', 'Death']
    
    ax2 = plt.subplot(325)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == str(i), 0], X_2d[target_ids == str(i), 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' + str(crop) + "_Survival" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)    
    

    plt.tight_layout()
    plt.show()
    
    plt.savefig('../image_features/' + name_out + "_median.pdf")   # save the figure to file
    plt.close()    # close the figure

    '''
    df = df.astype(float)
    img = df.corr()

    plt.imshow(img, origin = 'lower')
    plt.title('Correlation')
    plt.legend()
    plt.tight_layout()
    
    plt.savefig('../image_features/' + name_out + "_correlation.pdf")
    plt.close()
    '''
    
if __name__ == "__main__":
    
    #models = ["VGG", "Inception", "ResNet", "PCA", "SVD"]
    models = ["MobileNet", "ResNeXt"]
    crops = [512, 256]
    
    for model in models:
        for crop in crops: 
            
            
#        df, df2 = obtain_npy(model, crop = crop)
#        df.to_csv(model + '_' + str(crop) + "_median.csv")

#            #aggregate median
#            Classification_perf(df, model, crop = crop, name_out = model + '_' + str(crop) )

            #first image
            #Classification_perf(df2, model, crop = crop, name_out = model + '_' + str(crop) + 'First_image' )


            #model complexity remove 50% featueres -- columns
            df = obtain_npy(model, crop = crop)
            df.to_csv(model + '_' + str(crop) + "_median.csv")
            
            Classification_perf(df, model, crop = crop, name_out = model + '_' + str(crop) )
            
#            
#            df02 = df[list(np.random.choice(df.shape[1], df.shape[1] // 2, replace=False))]
#            
#            Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '0.5_Features' )

#             df02 = df[list(np.random.choice(df.shape[1], 10, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_10_Features_1' )

#             df02 = df[list(np.random.choice(df.shape[1], 10, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_10_Features_2' )


#             df02 = df[list(np.random.choice(df.shape[1], 15, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_15_Features_1' )

#             df02 = df[list(np.random.choice(df.shape[1], 15, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_15_Features_2' )


#             df02 = df[list(np.random.choice(df.shape[1], 25, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_25_Features_1' )

#             df02 = df[list(np.random.choice(df.shape[1], 25, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_25_Features_2' )

            
#             df02 = df[list(np.random.choice(df.shape[1], 50, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_50_Features_1' )

#             df02 = df[list(np.random.choice(df.shape[1], 50, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_50_Features_2' )
             

#             df02 = df[list(np.random.choice(df.shape[1], 100, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_100_Features_1' )

#             df02 = df[list(np.random.choice(df.shape[1], 100, replace=False))]
            
#             Classification_perf(df02, model, crop = crop, name_out = model + '_' + str(crop) + '_100_Features_2' )

            #no AUG
#            df2 = obtain_npy_NoAUG(model, crop = crop)
#            #df.to_csv(model + '_' + str(crop) + "_median.csv")
#            
#            Classification_perf(df2, model, crop = crop, name_out = model + '_' + str(crop) + 'NoAUG_image' )
#            

            
                        



