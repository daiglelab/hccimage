#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 10:57:27 2019

@author: llu
"""

import numpy as np
import pandas as pd
import math
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure
from scipy.stats import pearsonr
from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import FloatVector
from scipy import interp
from matplotlib.pyplot import figure


import os
os.chdir("/home/llu/HardDisk/Bitbucket/HCC_image/")
import clustering
from clustering import cluster_perf


import os
os.chdir("/home/llu/HardDisk/TCGA_HC_image/results/image_corr")

def read_image_feature(model = 'VGG', crop = 512):
    
    filename = model+ '_' + str(crop) + '_median.csv'
    tmp = pd.read_csv('../image_features/' + filename)
    tmp['Group'] = pd.Series([ one[:12] for one in tmp.iloc[:, 0].tolist() ])
    tmp = tmp.groupby(['Group']).mean()
    
    return(tmp)
    
    
def read_gene_path():
    
    filename = 'inferredPathwayLevels_RNAseq_copynumber.txt'
    tmp = pd.read_csv('../RNA_seq_data/' + filename, sep='\t', index_col = 0 )    
    return(tmp.T)    


def obtain_corr(subdf1, df2, model = "VGG", crop = 512, name_out = "test" ):
    
    df1 = subdf1.astype(float)
    img1 = df1.corr()
    
    df2 = df2.astype(float)
    img2 = df2.corr()
    
#    ax1 = plt.subplot(321)
#    ax1.imshow(img1, origin = 'lower')
#    ax1.set_title('Feature Correlation in image df1')
#    
#    img1 = df1.T.corr()
#    ax1 = plt.subplot(322)
#    ax1.imshow(img1, origin = 'lower')
#    ax1.set_title('Sample Correlation in image df1')    
#    
#    ax1 = plt.subplot(323)
#    ax1.imshow(img2, origin = 'lower')
#    ax1.set_title('Feature Correlation in gene pathway')
#    
#    img2 = df2.T.corr()
#    ax1 = plt.subplot(324)
#    ax1.imshow(img2, origin = 'lower')
#    ax1.set_title('Sample Correlation in gene pathway') 
#    
    coeffmat = np.zeros((df1.shape[1], df2.shape[1]))
    pvalmat = np.zeros((df1.shape[1], df2.shape[1]))
    
    for i in range(df1.shape[1]):    
        for j in range(df2.shape[1]):        
            corrtest = pearsonr(df1[df1.columns[i]], df2[df2.columns[j]])  

            coeffmat[i,j] = corrtest[0]
            pvalmat[i,j] = corrtest[1]

    df1.columns = [model + "_" + str(crop) + "_" + one for one in df1.columns.tolist()]
    dfcoeff = pd.DataFrame(coeffmat, columns=df2.columns, index=df1.columns)
    dfpval = pd.DataFrame(pvalmat, columns=df2.columns, index=df1.columns)
    
    df_all = pd.melt(dfcoeff.reset_index(), id_vars = 'index')
    df_all['Pvalue'] = pd.melt(dfpval.reset_index(), id_vars = 'index').iloc[:, -1]
    df_all.columns = ["Image Features", "Pathway", "Corr", "Pvalue"]
    
    stats = importr('stats')
    pvalue_list = df_all['Pvalue'].tolist()
    p_adjust = stats.p_adjust(FloatVector(pvalue_list), method = 'BH')
    df_all['Adj.P']  = p_adjust
    
    name_out = name_out + '_all_' + str(df_all.shape[0])
    
    df_all = df_all[df_all['Adj.P'] < 0.05]
    
    name_out = name_out + '_sig_' + str(df_all.shape[0])
    #df_all = df_all[df_all['Corr'] > 0.4 or df_all['Corr'] < -0.4]
    df_all = df_all.sort_values(by=['Adj.P'])
    print(df_all.shape)
    
    ##unique features and unique pathways
    unique_features = df_all["Image Features"].tolist()
    unique_paths = df_all["Pathway"].tolist()    
    
    #np.unique(unique_features, return_counts=True)
    #np.unique(unique_paths, return_counts=True)
    '''
    import networkx as nx
    import matplotlib.pyplot as plt
    
    G = nx.Graph()
    
    count = 20
    nodes = np.unique(df_all["Image Features"].tolist()[:count] + df_all["Pathway"].tolist()[:count]).tolist()
    edges = [(i, j) for i, j in zip(df_all["Image Features"].tolist()[:count], df_all["Pathway"].tolist()[:count])]
    
    G.add_nodes_from(nodes)
    G.add_edges_from(edges) 
    
    plt.subplot(121)
    nx.draw(G, with_labels=True, font_weight='bold')
    print(nx.info(G))
    '''
    
    
    figure(num=None, figsize=(10, 12), dpi=80, facecolor='w', edgecolor='k')
    ax1 = plt.subplot(211)
    ax1.plot(range(df_all.shape[0]), df_all["Corr"], 'r.' , label = 'Corr')
    ax1.plot(range(df_all.shape[0]), df_all["Adj.P"], 'b.', label = 'Adj.P')
    #ax1.set_xlabel("Number of clustering")
    ax1.set_ylabel("Values")   
    ax1.set_title(model + '_' + str(crop) + '_Corr_0.05_all')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    ax1.grid(True)
    
    ax1 = plt.subplot(212)
    ax1.plot(range(100), df_all["Corr"][:100], 'r.' , label = 'Corr')
    ax1.plot(range(100), df_all["Adj.P"][:100], 'b.', label = 'Adj.P')
    #ax1.set_xlabel("Number of clustering")
    ax1.set_ylabel("Values")   
    ax1.set_title(model + '_' + str(crop) + '_Corr_0.05_top100')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    ax1.grid(True)
    
    plt.tight_layout()
    
    plt.savefig('../image_corr/'+ name_out + "_Path_correlation.pdf")
    plt.close()
    
    ##save the sig over number here
    
    df_all.to_csv('../image_corr/'+ name_out + '_corr_sig_all.csv')
    
#    import networkx as nx
#    plt.subplot(111)
#    G = nx.petersen_graph()
#    for i in range(df_sub.iloc[:10, :]):
#         G.add()
    
#    G.add_node(1)
    #G.add_nodes_from([2, 3])
    #G.add_edges_from([(1, 2), (1, 3)])
#    
#    nx.draw(G, with_labels=True, font_weight='bold')
#    plt.show()
    return df_all
    
    
    
'''
correlation between features of samples from images and pathway

'''

def main_subgroup(model, crop):
    
    #model = 'VGG'; crop = 512
    df1 = read_image_feature(model,  crop)
    df1.to_csv(model + '_' + str(crop) + '_median.csv')
    
    tmp = pd.read_csv('../Survival/' + model + '_' + str(crop) + '/' + model + '_' + str(crop) + '_uni_surv_image_sig.txt', index_col = 0, sep='\t' )  
    print('Uni sig regression at OS:', tmp.loc[tmp['OS p.value'] < 0.05, :].shape, 'Uni sig regression group:', tmp.loc[tmp['DFS p.value'] < 0.05, :].shape)
    idx = [one.replace('X', '') for one in tmp.index.tolist()]
    
    #Union
    subdf1 = df1[idx] 
    
    #intersection
    tmp2 = tmp.loc[tmp['OS p.value'] < 0.05, :]    
    tmp2 = tmp2.loc[tmp2['DFS p.value'] < 0.05, :]
    idx = [one.replace('X', '') for one in tmp2.index.tolist()]
        
    subdf2 = df1[idx]    
    
    print(df1.shape, subdf1.shape, subdf2.shape)
    
    #test all, union, intersection
    cluster_perf(df1, subdf1, subdf2, model, crop, name_out = model + '_' + str(crop) )
    
    #return(tmp)    


def main_corr(model = 'VGG', crop = 256, name_out = model + '_' + str(crop)):
    
    #model = 'VGG'; crop = 512
    df1 = read_image_feature(model,  crop)
    df1.to_csv(model + '_' + str(crop) + '_median.csv')
    
    df2 = read_gene_path()
    print(df1.shape, df2.shape)
    # (405, 1408) (371, 5745)
    idx = list(set(df1.index.tolist()).intersection(df2.index.tolist()))
    df1 = df1.loc[idx, :]
    df2 = df2.loc[idx, :]
        
    #Out[18]: df2.shape (356, 5775)
    # remove low variance at pathways
    var = df2.var().tolist()
    
    #df2 = df2.iloc[:, [i for i, one in enumerate(var) if one > np.median(var)] ]
    
    genes = [int(one.split("_")[0]) for one in df2.columns.tolist()]
    #select top 1000 TRUE and FALSE
    tops = [index for index, value in sorted(enumerate(genes), reverse=True, key=lambda x: x[1]) if value > 1]
    tops = [df2.columns.tolist()[one] for one in tops]
    idx = [i for i, j in enumerate(genes) if j > 20 ]
    
    ##from sig results in subgroup DE pathways
    #idx = ['66_IL8', '66_ICAM1', '19_EPHB3', '19_ROCK1', '19_Ephrin B1/EPHB3']
    
    #df2 = df2.loc[:, idx] 
    print(df1.shape, df2.shape)
    #df2.to_csv('Pathway_clean.csv')
    
    # add clustering
    print(df1.shape, df2.shape)
    # (371, 1408) (371, 1000)
    # only consider survival significantly related image features
    tmp = pd.read_csv('../Survival/' + model + '_' + str(crop) + '/' + model + '_' + str(crop) + '_uni_surv_image_sig.txt', index_col = 0, sep='\t' )  
    print('Uni sig regression at OS:', tmp.loc[tmp['OS p.value'] < 0.05, :].shape, 'Uni sig regression group:', tmp.loc[tmp['DFS p.value'] < 0.05, :].shape)

    idx = [one.replace('X', '') for one in tmp.index.tolist()]
    
    #Union
    #subdf1 = df1[idx] 
    
    #intersection
    tmp2 = tmp.loc[tmp['OS p.value'] < 0.05, :]    
    tmp2 = tmp2.loc[tmp2['DFS p.value'] < 0.05, :]
    idx = [one.replace('X', '') for one in tmp2.index.tolist()]    
    
    subdf1 = df1[idx]

    print(subdf1.shape, df2.shape)    
    
    tmp = obtain_corr(subdf1, df2, model = model, crop = crop, name_out = name_out )
    
    return tmp
    


if __name__ == "__main__":
    
    models = ["VGG", "ResNet", "Inception"]
    crops = [256, 512]

    df_out = pd.DataFrame()  
    for model in models:
        for crop in crops:
            # model = 'VGG'; crop = 512
            #tmp = main_subgroup(model, crop)
            
            tmp = main_corr(model, crop, name_out = model + '_' + str(crop))
            df_out = df_out.append(tmp)
    
    #cat *_Clustering_count.csv > "All_Clustering_count_all.csv"
    #os.system('cat ../Clustering/*_Clustering_count.csv > ../Clustering/All_Clustering_count_all.csv')
    # Count how many image features are correlated with pathways
    tmp = list(set(df_out['Image Features'].tolist()))
    tmp2 = [ one.split('_')[0] + "-" + one.split('_')[1] for one in tmp]
    np.unique(tmp2, return_counts = True)
    
    np.max(df_out['Corr']) # 0.3846095934123655
    np.min(df_out['Corr']) #Out[23]: -0.5359336844168203
    #(array(['Inception-256', 'Inception-512', 'ResNet-256', 'ResNet-512',
    #    'VGG-256', 'VGG-512'], dtype='<U13'),
    #array([199, 119, 192,  94,  90,  89]))
    
    df_out.to_csv("../image_corr/All_correlation_count_all.csv")
    
    df_out = df_out.sort_values(by=['Adj.P'])
    
    ###top
    count = 100
    network = df_out.iloc[:count, :]
    #edge and node files
    edges = network
    
    nodes_c = np.unique(network["Image Features"].tolist()[:count] + network["Pathway"].tolist()[:count]).tolist()
    
    nodes = pd.DataFrame(index = range(len(nodes_c)), columns = ['oldnames', 'names', 'type', 'model', 'crop', 'Feature_number', 'number'] )
    nodes['oldnames'] = nodes_c
    
    nodes['type'] = ["Pathway" if one.startswith('1') else one.split("_")[0] for one in nodes_c]
    
    nodes['names'] = [one.split("_")[0] if not one[0].isdigit() else "Pathway" for one in nodes_c]

    nodes['Feature_number'] = [one.split("_")[2] if not one[0].isdigit() else "Pathway" for one in nodes_c]    
    
    nodes['number'] = [one.split("_")[1] if not one[0].isdigit() else "Pathway" for one in nodes_c]    
            
    edges.to_csv("../image_corr/All_corrlation_top_edges.csv")
    nodes.to_csv("../image_corr/All_corrlation_top_nodes.csv")    
    
    
    ##five pathways
    idx = ['66_IL8', '66_ICAM1', '19_EPHB3', '19_ROCK1', '19_Ephrin B1/EPHB3']
    tmp2 = df_out[df_out['Pathway'].isin(idx)]
    tmp2 = tmp2[tmp2['Adj.P'] < 0.05 ]
    
    tmp00 = list(set(tmp2['Image Features'].tolist()))
    tmp20 = [ one.split('_')[0] + "-" + one.split('_')[1] for one in tmp00]
    np.unique(tmp20, return_counts = True)
    
    #(array(['Inception-256', 'Inception-512', 'ResNet-256', 'ResNet-512',
    #'VGG-256', 'VGG-512'], dtype='<U13'), array([16, 15, 11,  3,  2,  2])) total 49
        
    #edge and node files
    edges = tmp2
    
    nodes_c = np.unique(tmp2["Image Features"].tolist()[:count] + tmp2["Pathway"].tolist()[:count]).tolist()
    
    nodes = pd.DataFrame(index = range(len(nodes_c)), columns = ['oldnames', 'names', 'type', 'model', 'crop', 'feature_number', 'number'] )
    nodes['oldnames'] = nodes_c
    
    nodes['type'] = [one.split("_")[0] + '_' + str(one.split("_")[1]) if not one[0].isdigit() else "Pathway" for one in nodes_c]
    
    nodes['names'] = [one.split("_")[0] if not one[0].isdigit() else "Pathway" for one in nodes_c]

    nodes['Feature_number'] = [one.split("_")[2] if not one[0].isdigit() else one for one in nodes_c]    
    
    nodes['number'] = [one.split("_")[1] if not one[0].isdigit() else "Pathway" for one in nodes_c]    
            
    edges.to_csv("../image_corr/5pathways_corrlation_top_edges.csv")
    nodes.to_csv("../image_corr/5pathways_corrlation_top_nodes.csv")      
    
    
    
    
    
    
    
    
    
    
    
    
    