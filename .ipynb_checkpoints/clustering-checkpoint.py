#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 14:25:50 2019

@author: llu
"""

print(__doc__)

# Author: Phil Roth <mr.phil.roth@gmail.com>
# License: BSD 3 clause

import os
os.chdir("/home/llu/HardDisk/Bitbucket/HCC_image/results/Clustering")

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve
from scipy import interp
from matplotlib.pyplot import figure


from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.metrics import davies_bouldin_score, silhouette_score

def cluster_perf(df1, subdf1, subdf2, model, crop, name_out = 'test' ):
    
    random_state = 170
    # select the best models
    out = pd.DataFrame(index = range(30), columns = ["DataSet", "Crop_size","DataUse", "Kmean_n", "Samples", "Silhouette", "davies_bouldin", "Labels", "SampleNames"] )
    out['SampleNames'] = ";".join(df1.index.tolist())
    
    X = np.array(df1)
    
    for kk in range(10):
        
        kk_n = kk + 2
        kmeans = KMeans(n_clusters = kk_n, random_state=random_state).fit(X)
        y_pred = kmeans.labels_
        
        ss = silhouette_score(X, y_pred, metric='sqeuclidean')
        db = davies_bouldin_score(X, y_pred) 

        samplen = np.unique(y_pred, return_counts = True)[1].tolist()
        samplen = "-".join([str(one) for one in samplen])
        mylabel = ";".join([str(one) for one in y_pred])
        
        out.iloc[kk, :-1] = [model, str(crop), "All", kk_n, samplen, ss, db, mylabel]
    
    ###plot ss and db
    figure(num=None, figsize=(21, 18), dpi=80, facecolor='w', edgecolor='k')
    #plt.tick_params(labelsize=24)
    #plt.xticks(fontsize=24)

    out_tmp = out.loc[out['DataUse'] =="All", ["Kmean_n", "Silhouette", "davies_bouldin"]]   
    #out_tmp = out_tmp.melt(id_vars=["Kmean_n"])
    ax1 = plt.subplot(331)
    
    #ax = sns.lineplot(x="Kmean_n", y="value", hue="variable", data=out_tmp)
    ax1.plot(out_tmp['Kmean_n'], out_tmp['Silhouette'], 'ro', linestyle='--', lw=2, label = 'Silhouette')
    ax1.plot(out_tmp['Kmean_n'], out_tmp['davies_bouldin'],'bs', linestyle='--', lw=2, label = 'davies_bouldin')    
    ax1.set_xlabel("Number of clustering")
    ax1.set_ylabel("Values")   
    ##ax1.set_title(model + '_' + str(crop) + '_Kmeans_perf')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    ax1.grid(True)
     
    #select the best
    best1 = out_tmp[out_tmp["Silhouette"] == out_tmp["Silhouette"].max()].iloc[0, 0]
    kmeans = KMeans(n_clusters = best1, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
    
    ss = silhouette_score(X, y_pred, metric='sqeuclidean')
    db = davies_bouldin_score(X, y_pred)     
    
    target_ids = np.array(y_pred)
    target_names = np.unique(target_ids).tolist()    
    
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    x = StandardScaler().fit_transform(X)
    pca = PCA(n_components=10)
    principalComponents = pca.fit_transform(x)
    pca.explained_variance_ratio_
    principalDf = principalComponents
    
    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(principalDf)
    X_2d.shape
        
    ax1 = plt.subplot(332)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    ##ax1.set_title(model + '_PCA10_T-SNE_' + str(crop) + '_Subgroups_all_KMeans')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    #plt.show()
    
    ###############classification
    X = np.array(df1)
    kmeans = KMeans(n_clusters = 2, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
        
    y = y_pred    
    random_state = np.random.RandomState(0)
    cv = StratifiedKFold(n_splits=6)
    #cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    
    classifier = svm.SVC(kernel='linear', probability=True, random_state=random_state)
    
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    
    ax1 = plt.subplot(333)
    i = 0
    for train, test in cv.split(X, y):

        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        ax1.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
        
    ax1.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    
    ax1.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC(AUC=%0.2f$\pm$%0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax1.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$1 std. dev.')
    
    ax1.set_xlim([-0.05, 1.05])
    ax1.set_ylim([-0.05, 1.05])
    ax1.set_xlabel('False Positive Rate')
    ax1.set_ylabel('True Positive Rate')
    ##ax1.set_title('Receiver operating characteristic')
    ax1.legend(loc="lower right")
    #ax1.show()

    ######################################################################3
    ## add subplot
    X = np.array(subdf1)
    for kk in range(10):
        
        kk_n = kk + 2
        kmeans = KMeans(n_clusters = kk_n, random_state=random_state).fit(X)
        y_pred = kmeans.labels_
        
        ss = silhouette_score(X, y_pred, metric='sqeuclidean')
        db = davies_bouldin_score(X, y_pred) 

        samplen = np.unique(y_pred, return_counts = True)[1].tolist()
        samplen = "-".join([str(one) for one in samplen])
        mylabel = ";".join([str(one) for one in y_pred])
        
        out.iloc[kk + 10, :-1] = [model, str(crop), "Union", kk_n, samplen, ss, db, mylabel]
    
    
    ###plot ss and db
    out_tmp = out.loc[out['DataUse'] =="Union", ["Kmean_n", "Silhouette", "davies_bouldin"]]   
    #out_tmp = out_tmp.melt(id_vars=["Kmean_n"])
    ax1 = plt.subplot(334)
    
    #ax = sns.lineplot(x="Kmean_n", y="value", hue="variable", data=out_tmp)
    ax1.plot(out_tmp['Kmean_n'], out_tmp['Silhouette'], 'ro', linestyle='--', lw=2, label = 'Silhouette')
    ax1.plot(out_tmp['Kmean_n'], out_tmp['davies_bouldin'],'bs', linestyle='--', lw=2, label = 'davies_bouldin')    
    ax1.set_xlabel("Number of clustering")
    ax1.set_ylabel("Values")   
    #ax1.set_title(model + '_' + str(crop) + '_Kmeans_perf')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    ax1.grid(True)
         
    #select the best
    best1 = out_tmp[out_tmp["Silhouette"] == out_tmp["Silhouette"].max()].iloc[0, 0]
    kmeans = KMeans(n_clusters = best1, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
    
    ss = silhouette_score(X, y_pred, metric='sqeuclidean')
    db = davies_bouldin_score(X, y_pred)     
    
    target_ids = np.array(y_pred)
    target_names = np.unique(target_ids).tolist()    
    
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    x = StandardScaler().fit_transform(X)
    pca = PCA(n_components=10)
    principalComponents = pca.fit_transform(x)
    pca.explained_variance_ratio_
    principalDf = principalComponents
    
    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(principalDf)
    X_2d.shape
        
    ax1 = plt.subplot(335)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    #ax1.set_title(model + '_PCA10_T-SNE_' + str(crop) + '_Subgroups_Sub_KMeans')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ###############classification
    X = np.array(subdf1)
    kmeans = KMeans(n_clusters = 2, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
        
    y = y_pred    
    random_state = np.random.RandomState(0)
    cv = StratifiedKFold(n_splits=6)
    #cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    
    classifier = svm.SVC(kernel='linear', probability=True, random_state=random_state)
    
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    
    ax1 = plt.subplot(336)
    i = 0
    for train, test in cv.split(X, y):

        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        ax1.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
        
    ax1.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    
    ax1.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC(AUC=%0.2f$\pm$%0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax1.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$1 std. dev.')
    
    ax1.set_xlim([-0.05, 1.05])
    ax1.set_ylim([-0.05, 1.05])
    ax1.set_xlabel('False Positive Rate')
    ax1.set_ylabel('True Positive Rate')
    ##ax1.set_title('Receiver operating characteristic')
    ax1.legend(loc="lower right")
    
    ########################intersection
    ######################################################################3
    ## add subplot
    X = np.array(subdf2)
    for kk in range(10):
        
        kk_n = kk + 2
        kmeans = KMeans(n_clusters = kk_n, random_state=random_state).fit(X)
        y_pred = kmeans.labels_
        
        ss = silhouette_score(X, y_pred, metric='sqeuclidean')
        db = davies_bouldin_score(X, y_pred) 

        samplen = np.unique(y_pred, return_counts = True)[1].tolist()
        samplen = "-".join([str(one) for one in samplen])
        mylabel = ";".join([str(one) for one in y_pred])
        
        out.iloc[kk + 20, :-1] = [model, str(crop), "Intersection", kk_n, samplen, ss, db, mylabel]
    
    
    ###plot ss and db
    out_tmp = out.loc[out['DataUse'] =="Intersection", ["Kmean_n", "Silhouette", "davies_bouldin"]]   
    #out_tmp = out_tmp.melt(id_vars=["Kmean_n"])
    ax1 = plt.subplot(337)
    
    #ax = sns.lineplot(x="Kmean_n", y="value", hue="variable", data=out_tmp)
    ax1.plot(out_tmp['Kmean_n'], out_tmp['Silhouette'], 'ro', linestyle='--', lw=2, label = 'Silhouette')
    ax1.plot(out_tmp['Kmean_n'], out_tmp['davies_bouldin'],'bs', linestyle='--', lw=2, label = 'davies_bouldin')    
    ax1.set_xlabel("Number of clustering")
    ax1.set_ylabel("Values")   
    #ax1.set_title(model + '_' + str(crop) + '_Kmeans_perf')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    ax1.grid(True)
         
    #select the best
    best1 = out_tmp[out_tmp["Silhouette"] == out_tmp["Silhouette"].max()].iloc[0, 0]
    kmeans = KMeans(n_clusters = best1, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
    
    ss = silhouette_score(X, y_pred, metric='sqeuclidean')
    db = davies_bouldin_score(X, y_pred)     
    
    target_ids = np.array(y_pred)
    target_names = np.unique(target_ids).tolist()    
    
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    x = StandardScaler().fit_transform(X)
    pca = PCA(n_components=10)
    principalComponents = pca.fit_transform(x)
    pca.explained_variance_ratio_
    principalDf = principalComponents
    
    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(principalDf)
    X_2d.shape
        
    ax1 = plt.subplot(338)
    
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    #ax1.set_title(model + '_PCA10_T-SNE_' + str(crop) + '_Subgroups_Sub_KMeans')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ###############classification
    X = np.array(subdf1)
    kmeans = KMeans(n_clusters = 2, random_state=random_state).fit(X)
    y_pred = kmeans.labels_
        
    y = y_pred    
    random_state = np.random.RandomState(0)
    cv = StratifiedKFold(n_splits=6)
    #cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    
    classifier = svm.SVC(kernel='linear', probability=True, random_state=random_state)
    
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    
    ax1 = plt.subplot(339)
    i = 0
    for train, test in cv.split(X, y):

        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        ax1.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1
        
    ax1.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    
    ax1.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC(AUC=%0.2f$\pm$%0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)
    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax1.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$1 std. dev.')
    
    ax1.set_xlim([-0.05, 1.05])
    ax1.set_ylim([-0.05, 1.05])
    ax1.set_xlabel('False Positive Rate')
    ax1.set_ylabel('True Positive Rate')
    ##ax1.set_title('Receiver operating characteristic')
    ax1.legend(loc="lower right")
     
    #####################################
    #ax1.show()    
    plt.rc('font', size = 24 )          # controls default text sizes
    plt.rc('axes', titlesize = 24)     # fontsize of the axes title
    plt.rc('axes', labelsize = 24)    # fontsize of the x and y labels
    plt.rc('legend', fontsize= 12 )
    
    #plt.show()
    plt.tight_layout()

    plt.savefig('../Clustering/' + name_out + "_Clustering.pdf", dpi=300)
    
    out.to_csv('../Clustering/' + name_out + "_Clustering_count.csv")

    #print(out)
    
    return(out)
    







