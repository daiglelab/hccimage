

import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure

import os
os.chdir("/home/llu/HardDisk/TCGA_HC_image/results/RNA_seq_data")

def read_path():
    
    mydir = "../../HCC_data/RNASeq/gdac.broadinstitute.org_LIHC-TP.Pathway_Paradigm_RNASeq.Level_4.2016012800.0.0/"
    filename = "inferredPathwayLevels.tab"            
    tmp = pd.read_table(mydir + filename)
    tmp.index = tmp.iloc[:, 0]
    tmp = tmp.iloc[:, 1:].T
    tmp = tmp.loc[:, tmp.var() != 0]
    return tmp


def Clinical_df():
    
    filename = "../HCC_clinical/HCC_clinicalInfo.txt"
    out = pd.read_table(filename)
    return(out)


def TSNE_plot(df):

    tsne = TSNE(n_components=2, random_state=0)
    X_2d = tsne.fit_transform(df)
    X_2d.shape
    
    names = df.index.tolist()
    clinical = Clinical_df()
    col = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple']
    
    #target_ids = np.random.randint(2, size= 288)
    ###draw Cancer type
    target_ids = ["Cancer" for one in df.index.tolist() ]
    target_ids = np.array(target_ids)
    target_names = np.unique(target_ids).tolist()
    
    #PCA check varies
    x = StandardScaler().fit_transform(df)
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    pca.explained_variance_ratio_
    principalDf = np.array(principalDf)
    # array([ 0.19312921,  0.15181227])
    model = "Pathway"
    
    figure(num=None, figsize=(10, 12), dpi=80, facecolor='w', edgecolor='k')
    
    ax1 = plt.subplot(321)

    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(principalDf[target_ids == j, 0], principalDf[target_ids == j, 1], c = k, label = j )
    
    ax1.set_xlabel( "PCA1 {0:.1%}".format(pca.explained_variance_ratio_[0]) )
    ax1.set_ylabel( "PCA2 {0:.1%}".format(pca.explained_variance_ratio_[1]) )    
    ax1.set_title(model + '_PCA__DataType')
    ax1.legend()
    
    ax1 = plt.subplot(322)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax1.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )
    
    ax1.set_title(model + '_T-SNE_' + 'DataType')
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ###draw stage
    target_ids = [clinical.loc[clinical['Samples'] == one, 'Stage'].tolist()[0]  if one in clinical['Samples'].tolist() else np.NaN for one in names]
    target_ids = [one if not pd.isnull(one) else "No Stage" for one in target_ids]
    target_ids = np.array(target_ids)
    #cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    #target_ids[cancers == "Normal"] = "Normal"
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['Stage I', 'Stage II', 'Stage III', 'Stage IV']    
    ax2 = plt.subplot(323)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' +  "Stage" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    ###draw grade
    target_ids = [clinical.loc[clinical['Samples'] == one, 'Grade'].tolist()[0]  if one in clinical['Samples'].tolist() else np.NaN for one in names]
    target_ids = [one if not pd.isnull(one) else "No Grade" for one in target_ids]
    target_ids = np.array(target_ids)
    #cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    #target_ids[cancers == "Normal"] = "Normal"
    
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['G1', 'G2', 'G3', "G4"]
    
    ax2 = plt.subplot(324)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == j, 0], X_2d[target_ids == j, 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' + "Grade" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    
    # draw OS event
    target_ids = [clinical.loc[clinical['Samples'] == one, 'OS_event'].tolist()[0]  if one in clinical['Samples'].tolist() else np.NaN for one in names]
    target_ids = [one if not pd.isnull(one) else "No Event" for one in target_ids]
    target_ids = np.array(target_ids)
    #cancers = np.array(["Cancer" if one[13:15] == '01' else "Normal" for one in df.index.tolist() ])
    #target_ids[cancers == "Normal"] = "Normal"
    #target_names = ["Cancer", "Normal"]
    #col = ["r", "b"]
    target_names = ['Alive', 'Death']
    
    ax2 = plt.subplot(325)
    for i, j, k in zip(range(len(target_names)), target_names, col[:len(target_names)]):
        ax2.scatter(X_2d[target_ids == i, 0], X_2d[target_ids == i, 1], c = k, label = j )

    ax2.set_title(model + '_T-SNE_' + "Survival" )
    #plt.legend()
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)    
    
    plt.tight_layout()
    plt.show()
    
    plt.savefig(model + "_T-SNE.pdf")   # save the figure to file
    plt.close()    # close the figure


df = read_path()
TSNE_plot(df)
df.to_csv('RNASeq_path.csv')

df = df.astype(float)
img = df.corr()

plt.imshow(img, origin = 'lower')
plt.title('Correlation')
plt.legend()
plt.tight_layout()

plt.savefig("Feature_correlation.pdf") 













